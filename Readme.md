# Swagger API Documentation using Pelican

[![CC-BY-SA 4.0](https://i.creativecommons.org/l/by-sa/4.0/88x31.png "CC-BY-SA")](https://creativecommons.org/licenses/by-sa/4.0/)

This project is a demo project for the included pelican-plugin for parsing swagger.json-files and showing the API docs with the help of the Pelican static website generator.

Swagger (or OpenAPI) is an API description format, that can be used to generate client- or server-stubs so applications can be developed independantly of each other but still communicate via the API defined in the Swagger file. Most languages or applications ship their own code to present Swagger to the user.

This project uses pelican to generate the documentation beforehand to ship with your application or host independantly from it.

## How to use

1. Change the entries in the `pelicanconf.py` to your liking.
2. Place the `swagger.json` and `swagger.yaml` in the content-folder (for the plugin this is configurable, but they are also copied to the output folder in the Makefile)
3. Run make to generate the static API docs in www

## The pelican-swagger Plugin

Pelican swagger is a generator plugin, that generates articles from the `swagger.json` and appends them to the other articles found in content. Each path is a route-article and each object is an object-article. These are displayed on the index-page by the template.

The template is based on [pelican-blue](https://github.com/Parbhat/pelican-blue) and serves as a demo on how to use and query the generated articles to present the API docs.

## Contribute

The plugin and the provided theme are not feature complete. If you find anything, that is not present, but is useful to you, feel free to share your extentions and send a pull request.
