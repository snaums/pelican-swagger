var filterByMethod = function(method) {
    var lst = document.querySelectorAll(".swagger-route-method");

    for ( var i = 0; i < lst.length; i++ ) {
        var ls = lst[i].parentElement.parentElement.parentElement

        if (lst[i].textContent != method && method != "ALL") {
            ls.classList.add ( "hidden" )
        } else {
            while ( ls.classList.contains("hidden") ) {
                ls.classList.remove( "hidden" )
            }
        }
    }
}
