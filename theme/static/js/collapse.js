function collapse( e ) {
    var element=this;
    var body=document.querySelector("body");
    while ( !element.classList.contains("swagger-route") && !element.classList.contains("swagger-object") ) {
        element=element.parentNode;
        if ( body == element ) {
            return;
        }
    }

    for ( var i=0; i<element.children.length; i++ ) {
        var c = element.children[i];
        if ( c.classList.contains("swagger-route-details") || c.classList.contains("swagger-object-details") ) {
            if ( c.classList.contains("hidden") ) {
                c.classList.remove("hidden");
            } else {
                c.classList.add("hidden");
            }
            return;
        }
    }
}

function forceUncollapsed(e) {
    var selector = this.href.split("#")[1];
    uncollapse ( selector );
}

function uncollapse( selector ) {
    var body=document.querySelector("body");
    var element=document.getElementById ( selector );
    while ( !element.classList.contains("swagger-route") && !element.classList.contains("swagger-object") ) {
        element=element.parentNode;
        if ( body == element ) {
            return;
        }
    }

    for ( var i=0; i<element.children.length; i++ ) {
        var c = element.children[i];
        if ( c.classList.contains("swagger-route-details") || c.classList.contains("swagger-object-details") ) {
            c.classList.remove("hidden");
        }
    }
}

document.addEventListener("DOMContentLoaded", function () {
    var l = document.querySelectorAll (".swagger-route-link");
    l.forEach ( function (element) {
        element.addEventListener("click", collapse);
    });
    var lo = document.querySelectorAll (".swagger-object-link");
    lo.forEach ( function (element) {
        element.addEventListener("click", collapse);
    });
    var lnav = document.querySelectorAll(".subnav__link");
    lnav.forEach ( function(element) {
        element.addEventListener("click", forceUncollapsed);
    });
    var lresolved = document.querySelectorAll(".object_resolve");
    lresolved.forEach ( function(element) {
        element.addEventListener("click", forceUncollapsed);
    });

    uncollapse(location.hash.substr(1,location.hash.length));
});
