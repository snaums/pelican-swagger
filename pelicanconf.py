from datetime import *
CURRENTYEAR = datetime.now().year
TIMEOFDAY = datetime.now().strftime("%Y-%m-%d %H:%M")

AUTHOR = 'Stefan Naumann'
SITENAME = 'Swagger Test'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Rome'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

SWAGGER_DOC="swagger.json"

STATIC_PATHS=['static']
#LOGO="static/logo.png"

# Blogroll
LINKS = (
    ('All', '/index.html'),
    ('Docs', '/docs.html'),
    ('Objects', '/objects.html'),
    ('Routes', '/routes.html'),
)

# Social widget
#SOCIAL = (('Codeberg', ''),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

PLUGIN_PATHS = [ "plugins" ]
PLUGINS = [ 'better_codeblock_line_numbering', 'pelican-swagger', 'tipue_search' ]
TIPUE_SEARCH = True

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {
            'css_class': 'highlight', 'linenums': False
        },
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {}
        },
    'output_format': 'html5'
}
