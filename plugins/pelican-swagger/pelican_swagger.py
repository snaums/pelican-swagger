import datetime

from pelican import signals
from pelican.contents import Article
from pelican.readers import BaseReader
import os.path
import json

def parseSwagger(file):
    with open(file) as f:
        js = json.load(f)
    return js

def renderObject(articleGenerator, reader, swagger, obj):
    ctx = articleGenerator.context
    if obj not in swagger["definitions"]:
        return False

    base_path = swagger["basePath"]

    o = swagger["definitions"][obj]
    dic = {
        "author": reader.process_metadata("author", ctx["AUTHOR"]),
        "date": datetime.datetime.now(),
        "category": reader.process_metadata("category", "object"),
        "type": o["type"],
        "title": obj,
        "properties": o["properties"],
    }

    if "required" in o:
        dic["required"]: o["required"]

    content = o["description"] if "description" in o else ""

    slug="object_%s"%(obj.replace("/", "_"))
    dic["slug"] = slug
    newArticle = Article(content, dic)
    articleGenerator.articles.append(newArticle)

def renderRoute(articleGenerator, reader, swagger, route):
    ctx = articleGenerator.context
    if route not in swagger["paths"]:
        return False

    base_path = swagger["basePath"]

    r = swagger["paths"][route]
    for method in r:
        rt = r[method]
        dic = {
            "author": reader.process_metadata("author", ctx["AUTHOR"]),
            "date": datetime.datetime.now(),
            "category": reader.process_metadata("category", "route"),
            "method": method,
            "title": os.path.join(base_path, route),
        }

        if "tags" in rt:
            tags = ""
            for t in rt["tags"]:
                tags += t + ","
            dic["tags"] = reader.process_metadata("tags", tags[:-1])

        strings = [ "parameters", "responses", "description", "summary", "consumes", "produces", "security", "deprecated", "success", "failure" ]
        for s in strings:
            if s in rt:
                dic[s] = rt[s]

        content = dic["description"] if "description" in dic else ""

        slug="route_%s_%s"%(method, route.replace("/", "_"))
        dic["slug"] = slug
        newArticle = Article(content, dic)
        articleGenerator.articles.append(newArticle)

def renderMetaData(swagger, ctx):
    spec = swagger
    ctx["SWAGGER"] = spec["swagger"]
    ctx["SITENAME"] = spec["info"]["title"]
    ctx["SITE_DESCRIPTION"] = spec["info"]["description"]
    ctx["AUTHOR"] = spec["info"]["contact"]["name"]
    ctx["CONTACT_URL"] = spec["info"]["contact"]["url"]
    ctx["AUTHOR_EMAIL"] = spec["info"]["contact"]["email"]
    ctx["LICENSE"] = spec["info"]["license"]["name"]
    ctx["LICENSE_URL"] = spec["info"]["license"]["url"]
    ctx["VERSION"] = spec["info"]["version"]
    ctx["BASE_PATH"] = spec["basePath"]

def renderSwagger(articleGenerator):
    settings = articleGenerator.settings
    ctx = articleGenerator.context
    if "SWAGGER_DOC" not in settings:
        raise ValueError("SWAGGER_DOCS must be set to the swagger json file in your config")

    swagger=parseSwagger(os.path.join(settings["PATH"], settings["SWAGGER_DOC"]))
    renderMetaData(swagger, ctx)

    # Author, category, and tags are objects, not strings, so they need to
    # be handled using BaseReader's process_metadata() function.
    baseReader = BaseReader(settings)
    for path in swagger["paths"]:
        renderRoute ( articleGenerator, baseReader, swagger, path )

    for obj in swagger["definitions"]:
        renderObject ( articleGenerator, baseReader, swagger, obj )


def register():
    signals.article_generator_pretaxonomy.connect(renderSwagger)
