PY?=python3
PELICAN?=pelican
PELICANOPTS=

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/content
OUTPUTDIR=$(BASEDIR)/www
CONFFILE=$(BASEDIR)/pelicanconf.py
THEMEDIR=$(CURDIR)/theme

DEBUG ?= 0
ifeq ($(DEBUG), 1)
	PELICANOPTS += -D
endif

RELATIVE ?= 0
ifeq ($(RELATIVE), 1)
	PELICANOPTS += --relative-urls
endif

SERVER ?= "0.0.0.0"

PORT ?= 0
ifneq ($(PORT), 0)
	PELICANOPTS += -p $(PORT)
endif

all: html

html:
	"$(PELICAN)" -v "$(INPUTDIR)" -o "$(OUTPUTDIR)" -s "$(CONFFILE)" -t "$(THEMEDIR)" $(PELICANOPTS)
	-mv tipuesearch_content.js $(OUTPUTDIR)/
	-rm -r www/authors.html www/archives.html www/categories.html www/author
	-cp content/swagger.json www/swagger.json
	-cp content/swagger.yaml www/swagger.yaml

highlight:
	sed -i 's/#1DAEDF/#3b8dbd/g' www/theme/css/style.css
	sed -i 's/#2980b9/#263238/g' www/theme/css/style.css
	sed -i 's/#328cc7/rgba(1,1,1,.3)/g' www/theme/css/style.css
	sed -i 's/3px solid transparent/3px solid #df7e08/g' www/theme/css/style.css

clean:
	[ ! -d "$(OUTPUTDIR)" ] || rm -rf "$(OUTPUTDIR)"

regenerate:
	"$(PELICAN)" -r "$(INPUTDIR)" -o "$(OUTPUTDIR)" -s "$(CONFFILE)" $(PELICANOPTS)

serve:
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS) -p 8080

.PHONY: html help clean serve
